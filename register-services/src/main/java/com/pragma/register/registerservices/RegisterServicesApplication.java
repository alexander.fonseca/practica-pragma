package com.pragma.register.registerservices;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.netflix.eureka.server.EnableEurekaServer;

@EnableEurekaServer
@SpringBootApplication
public class RegisterServicesApplication {

	public static void main(String[] args) {
		SpringApplication.run(RegisterServicesApplication.class, args);
	}

}
