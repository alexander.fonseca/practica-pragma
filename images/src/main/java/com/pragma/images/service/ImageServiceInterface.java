package com.pragma.images.service;

import com.pragma.images.dto.ImageDTO;
import com.pragma.images.entity.Image;
import org.springframework.web.multipart.MultipartFile;

import java.util.List;

public interface ImageServiceInterface {

    public ImageDTO save(MultipartFile file);

    public List<ImageDTO> getAll();

    public ImageDTO delete(Image image);

}
