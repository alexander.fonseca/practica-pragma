package com.pragma.images.service;

import com.pragma.images.dto.ImageDTO;
import com.pragma.images.entity.Image;
import com.pragma.images.repository.ImageRepository;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import java.util.List;
import java.util.UUID;

@Service
public class ImageServiceImpl implements ImageServiceInterface {

    @Autowired
    private ImageRepository imageRepository;

    @Override
    public ImageDTO save(MultipartFile file) {
        Image image = Image.
                builder()
                .id(UUID.randomUUID().toString())
                .build();
        if (file == null || file.isEmpty()) {
            image.setContent("https://i.pravatar.cc/300");
            image.setType("url");
        } else {
            try {
                image.setContent(ImageDTO.convertFileToString(file));
                image.setType("base64");
            } catch (Exception e) {
                System.out.println(e.getMessage());
            }
        }
        return ImageDTO.convertImageToImageDTO(imageRepository.save(image));
    }

    @Override
    public List<ImageDTO> getAll() {
        return ImageDTO.convertListImagesToListImagesDTO(imageRepository.findAll());
    }

    @Override
    public ImageDTO delete(Image image) {
        imageRepository.delete(image);
        return ImageDTO.builder().build();
    }
}
