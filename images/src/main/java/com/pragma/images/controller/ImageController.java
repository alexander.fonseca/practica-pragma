package com.pragma.images.controller;

import com.pragma.images.dto.ImageDTO;
import com.pragma.images.entity.Image;
import com.pragma.images.service.ImageServiceInterface;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import java.util.List;
import java.util.UUID;

@RestController
@CrossOrigin(origins = "*",methods = {RequestMethod.GET,RequestMethod.POST,RequestMethod.PUT,RequestMethod.DELETE})
@RequestMapping(value = "/images")
public class ImageController {

    @Autowired
    private ImageServiceInterface imageServiceInterface;

    @GetMapping
    public List<ImageDTO> listImages(){
        return imageServiceInterface.getAll();
    }

    @PostMapping
    public ResponseEntity<ImageDTO> save(@RequestParam(value = "image",required = false) MultipartFile file){
        return ResponseEntity.ok(imageServiceInterface.save(file));
    }
}
