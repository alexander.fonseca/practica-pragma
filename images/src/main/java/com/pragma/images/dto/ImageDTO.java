package com.pragma.images.dto;

import com.pragma.images.entity.Image;
import lombok.*;
import org.apache.commons.io.FileUtils;
import org.springframework.web.multipart.MultipartFile;

import java.io.File;
import java.util.ArrayList;
import java.util.Base64;
import java.util.List;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class ImageDTO {

    private String content;

    private String type;

    public static ImageDTO convertImageToImageDTO(Image image){
        return ImageDTO.builder()
                .content(image.getContent())
                .type(image.getType())
                .build();
    }

    public static List<ImageDTO> convertListImagesToListImagesDTO(List<Image> images){
        List<ImageDTO> imageDTOS = new ArrayList<>();
        images.forEach(image -> {
            imageDTOS.add(ImageDTO.builder()
                    .content(image.getContent())
                    .type(image.getType())
                    .build());
        });
        return imageDTOS;
    }

    public static String convertFileToString(MultipartFile file) throws Exception{
        return Base64.getEncoder().encodeToString(file.getBytes());
    }

}
