package com.pragma.images.repository;

import com.pragma.images.entity.Image;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
public interface ImageRepository extends MongoRepository<Image,String> {
    public Optional<Image> findById(String id);
}
