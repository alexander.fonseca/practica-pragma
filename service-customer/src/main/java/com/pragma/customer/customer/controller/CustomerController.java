package com.pragma.customer.customer.controller;

import com.pragma.customer.customer.dto.CustomerDTO;
import com.pragma.customer.customer.entity.Customer;
import com.pragma.customer.customer.entity.TypeIdentification;
import com.pragma.customer.customer.error.Cause;
import com.pragma.customer.customer.service.CustomerService;
import com.pragma.customer.customer.validate.Validate;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.server.ResponseStatusException;

import javax.validation.Valid;

import java.util.List;

@RestController
@CrossOrigin(origins = "*",methods = {RequestMethod.GET,RequestMethod.POST,RequestMethod.PUT,RequestMethod.DELETE})
@RequestMapping(value = "/customers")
@Slf4j
public class CustomerController {

    @Autowired
    private CustomerService customerService;

    @GetMapping
    public ResponseEntity<List<CustomerDTO>> listCustomers(@RequestParam(name = "filter",required = false) String filter){
        List<CustomerDTO> customersList;
        if (filter != null){
            log.info("El valor de filter que llega es : " + filter);
            customersList = customerService.getAllCustomerLessThanOrGreaterThanOrEqual(filter.trim());
        }else{
            customersList = customerService.getAllCustomer();
        }
        return ResponseEntity.ok(customersList);
    }

    @GetMapping("/search/type/number")
    public ResponseEntity<Customer> searchCustomerByTypeDocumentAndNumber(@RequestParam(name = "typeDocument",required = false) TypeIdentification typeDocument, @RequestParam(name = "numberDocument",required = false) String numberDocument){
        int validation = Validate.validateIfTypeAndNumberIdentificationNotNull(typeDocument,numberDocument);
        Customer customer = null;
        if(validation == 1){
            customer = customerService.findCustomerByTypeAndNumberIdentification(typeDocument,numberDocument);
        }
        return (customer != null) ? ResponseEntity.ok(customer) : ResponseEntity.notFound().build();
    }

    @GetMapping("/{id}")
    public ResponseEntity<Customer> searchCustomerById(@PathVariable Long id){
        Customer customer = customerService.getCustomerById(id);
        return (customer != null) ? ResponseEntity.ok(customer) : ResponseEntity.notFound().build();
    }

    @PostMapping
    public ResponseEntity<Customer> createCustomer(@Valid @RequestBody Customer customer, BindingResult result){

        if (result.hasErrors()){
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST,Validate.formatErrorMessage(result));
        }
        Customer created = customerService.findCustomerByTypeAndNumberIdentification(customer.getTypeIdentification(),customer.getNumberIdentification());
        if (created != null || customer.getId() != null ){
            throw new Cause("El numero y tipo de documento registrado para este usuario ya se encuentra en la base de datos",HttpStatus.FOUND.value(),"Registro duplicado");
        }

        created = customerService.save(customer);
        return (created != null) ? ResponseEntity.status(HttpStatus.CREATED).body(created) : ResponseEntity.status(HttpStatus.BAD_REQUEST).build();
    }

    @PutMapping("/{id}")
    public ResponseEntity<Customer> updateCustomer(@PathVariable Long id,@RequestBody Customer customer){
        if (customerService.getCustomerById(id) == null){
            return ResponseEntity.notFound().build();
        }else if (customer.getId() == null){
            return ResponseEntity.status(HttpStatus.BAD_REQUEST).build();
        }
        Customer updated = customerService.save(customer);
        return (updated != null) ? ResponseEntity.status(HttpStatus.OK).body(updated) : ResponseEntity.status(HttpStatus.BAD_REQUEST).build();
    }

    @DeleteMapping("/{id}")
    public ResponseEntity<Customer> deleteCustomer(@PathVariable Long id){
        Customer delete = customerService.getCustomerById(id);
        if (delete == null){
            return ResponseEntity.notFound().build();
        }
        delete = customerService.delete(delete);
        return (delete != null) ? ResponseEntity.status(HttpStatus.OK).body(delete) : ResponseEntity.status(HttpStatus.BAD_REQUEST).build();
    }

}
