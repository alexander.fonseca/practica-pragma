package com.pragma.customer.customer.validate;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.pragma.customer.customer.entity.TypeIdentification;
import com.pragma.customer.customer.error.Cause;
import org.springframework.http.HttpStatus;
import org.springframework.validation.BindingResult;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

public class Validate {

    public static int validateIfTypeAndNumberIdentificationNotNull(TypeIdentification typeDocument, String numberDocument) {
        int result = 0;
        if (typeDocument != null && numberDocument != null) {
            result = 1;
        } else if (typeDocument != null && numberDocument == null) {
            throw new Cause("Para filtar por tipo y numero de documento hace falta que envie el numero de documento", HttpStatus.BAD_REQUEST.value(),"Faltan parametros");
        } else if (typeDocument == null && numberDocument != null) {
            throw new Cause("Para filtar por tipo y numero de documento hace falta que envie el tipo de documento",HttpStatus.BAD_REQUEST.value(),"Faltan parametros");
        }
        return result;
    }

    public static int validateIfAgeHaveSpecialFormat(String word) {
        int age = 0;
        if (word.startsWith("+", 0) || word.startsWith("-", 0)) {
            age = Integer.parseInt(word.substring(1, word.length()));
        } else if (word.endsWith("+") || word.endsWith("-")) {
            age = Integer.parseInt(word.substring(0, word.length() - 1));
        } else {
            age = Integer.parseInt(word);
        }
        return age;
    }

    public static String formatErrorMessage(BindingResult result) {
        List<Map<String, String>> errors = result.getFieldErrors().stream().map(err -> {
            Map<String, String> error = new HashMap<>();
            error.put(err.getField(), err.getDefaultMessage());
            return error;
        }).collect(Collectors.toList());

        ErrorMessage errorMessage = ErrorMessage.builder()
                .code("01")
                .messages(errors)
                .build();
        ObjectMapper mapper = new ObjectMapper();
        String jsonString = "";
        try {
            jsonString = mapper.writeValueAsString(errorMessage);
        } catch (JsonProcessingException e) {
            e.printStackTrace();
        }
        return jsonString;
    }

}
