package com.pragma.customer.customer.error;

import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;

import java.util.concurrent.ConcurrentHashMap;

@ControllerAdvice
@Slf4j
public class MasterException extends ResponseEntityExceptionHandler {

    private static final String INTERNAL_ERROR = "Ocurrio un error favor contactar al administrador.";

    @ExceptionHandler(Exception.class)
    public final ResponseEntity<Error> handleAllExceptions(Exception exception) {
        ResponseEntity<Error> result;
        Cause cause = (Cause) exception;
        String exception_name = exception.getClass().getSimpleName();
        String message = exception.getMessage();
        Integer code = cause.getCode();
        if (code != null) {
            Error error = new Error(cause.getType(), message);
            result = new ResponseEntity<Error>(error, HttpStatus.valueOf(code));
        }
        else {
            log.error(exception.getMessage(),exception.getCause());
            Error error = new Error(exception_name, INTERNAL_ERROR);
            result = new ResponseEntity<Error>(error, HttpStatus.INTERNAL_SERVER_ERROR);
        }
        return result;
    }



}
