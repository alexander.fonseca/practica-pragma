package com.pragma.customer.customer.error;

import lombok.Data;

@Data
public class Error {

    private final String typeException;
    private final String message;

    public Error(String typeException, String message) {
        this.typeException = typeException;
        this.message = message;
    }

}

