package com.pragma.customer.customer.validate;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;
import java.util.Map;
@Data @Builder @NoArgsConstructor @AllArgsConstructor
public class ErrorMessage {

    private String code;
    private List<Map<String,String>> messages;



}
