package com.pragma.customer.customer.service;

import com.pragma.customer.customer.dto.CustomerDTO;
import com.pragma.customer.customer.entity.Customer;
import com.pragma.customer.customer.entity.TypeIdentification;

import java.util.List;

public interface CustomerService {

    // Obtenemos el customer por un ID
    public Customer getCustomerById(Long id);
    // Obtiene todos los clientes registrados dentro del sistema
    public List<CustomerDTO> getAllCustomer();
    // Guarda y edita un cliente
    public Customer save(Customer customer);
    // Elimnar un cliente
    public Customer delete(Customer customer);
    // Buscar un cliente por tipo de identificacion
    public Customer findCustomerByTypeAndNumberIdentification(TypeIdentification typeIdentification, String number);
    // Obtenemos los clientes de menor edad a la que recibimos
    public List<CustomerDTO> getAllCustomerLessThanOrGreaterThanOrEqual(String filter);

}
