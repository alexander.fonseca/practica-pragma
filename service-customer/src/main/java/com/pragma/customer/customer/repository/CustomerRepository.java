package com.pragma.customer.customer.repository;

import com.pragma.customer.customer.entity.Customer;
import com.pragma.customer.customer.entity.TypeIdentification;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface CustomerRepository extends JpaRepository<Customer,Long> {

    public Customer findByTypeIdentificationAndNumberIdentification(TypeIdentification typeIdentification,String numberIdentification);

    public List<Customer> findByAge(int age);

    public List<Customer> findByAgeLessThan(int age);

    public List<Customer> findByAgeLessThanEqual(int age);

    public List<Customer> findByAgeGreaterThan(int age);

    public List<Customer> findByAgeGreaterThanEqual(int age);

}
