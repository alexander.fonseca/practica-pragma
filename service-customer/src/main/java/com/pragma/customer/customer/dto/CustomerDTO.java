package com.pragma.customer.customer.dto;

import com.pragma.customer.customer.entity.Customer;
import com.pragma.customer.customer.entity.TypeIdentification;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;


import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Positive;
import javax.validation.constraints.Size;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

@Data @Builder @NoArgsConstructor @AllArgsConstructor
public class CustomerDTO {

    private Long id;

    @NotEmpty
    private String fisrtname;

    @NotEmpty
    private String lastname;

    @NotNull
    private TypeIdentification typeIdentification;

    @NotEmpty
    @Size(min = 8)
    private String numberIdentification;

    @Positive
    private int age;

    @NotEmpty
    private String cityOfBirth;

    private Date createdAt;

    private Date updatedAt;

    public static CustomerDTO convertCustomerEntityToCustomerDTO(Customer customer){
        return CustomerDTO.builder()
                .id(customer.getId())
                .fisrtname(customer.getFirstname())
                .lastname(customer.getLastname())
                .typeIdentification(customer.getTypeIdentification())
                .numberIdentification(customer.getNumberIdentification())
                .age(customer.getAge())
                .cityOfBirth(customer.getCityOfBirth())
                .createdAt(customer.getCreatedAt())
                .updatedAt(customer.getUpdatedAt())
                .build();
    }

    public static List<CustomerDTO> formatListCustomerToDTO(List<Customer> customers){
        List<CustomerDTO> customerDTO = new ArrayList<>();
        customers.forEach(customer->{
            if (!customer.getStatus().equalsIgnoreCase("deleted")){
                customerDTO.add(convertCustomerEntityToCustomerDTO(customer));
            }
        });
        return  customerDTO;
    }

}
