package com.pragma.customer.customer.error;

import lombok.Data;

public class Cause extends RuntimeException {

    private static final long serialVersionUID = 1L;

    private int code;
    private String type;

    public Cause(String message, int code,String type) {
        super(message);
        this.code = code;
        this.type = type;
    }

    public int getCode() {
        return code;
    }

    public String getType() {
        return type;
    }
}

