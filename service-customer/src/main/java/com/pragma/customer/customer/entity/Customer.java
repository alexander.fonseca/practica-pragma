package com.pragma.customer.customer.entity;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Positive;
import javax.validation.constraints.Size;
import java.io.Serializable;
import java.util.Date;

@Table(name = "tbl_customer")
@Entity
@Data
@Builder @NoArgsConstructor @AllArgsConstructor
public class Customer implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(nullable = false)
    @NotEmpty
    private String firstname;

    @Column(nullable = false)
    @NotEmpty
    private String lastname;

    @Column(name="type_identification")
    @Enumerated(EnumType.STRING)
    @NotNull
    private TypeIdentification typeIdentification;

    @Column(nullable = false)
    @NotEmpty
    @Size(min = 8)
    private String numberIdentification;

    @Column(nullable = false)
    @Positive
    private int age;

    @Column(nullable = false,name = "city_of_birth")
    @NotEmpty
    private String cityOfBirth;

    @Column(name = "image_id")
    private String imageId;
    
    private String status;

    @Column(name = "created_at")
    @Temporal(TemporalType.TIMESTAMP)
    private Date createdAt;

    @Column(name = "updated_at")
    @Temporal(TemporalType.TIMESTAMP)
    private Date updatedAt;

}
