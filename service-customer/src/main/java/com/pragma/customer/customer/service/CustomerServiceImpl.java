package com.pragma.customer.customer.service;

import com.pragma.customer.customer.dto.CustomerDTO;
import com.pragma.customer.customer.entity.Customer;
import com.pragma.customer.customer.entity.TypeIdentification;
import com.pragma.customer.customer.repository.CustomerRepository;
import com.pragma.customer.customer.validate.Validate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

@Service
public class CustomerServiceImpl implements CustomerService {

    @Autowired
    private CustomerRepository customerRepository;

    @Override
    @Transactional(readOnly = true)
    public Customer getCustomerById(Long id){
        return customerRepository.findById(id).orElse(null);
    }

    @Override
    @Transactional(readOnly = true)
    public List<CustomerDTO> getAllCustomer() {
        return CustomerDTO.formatListCustomerToDTO(customerRepository.findAll());
    }

    @Override
    @Transactional
    public Customer save(Customer customer) {
        if (customer.getId() == null) {
            customer.setStatus("created");
            customer.setCreatedAt(new Date());
        } else {
            customer.setCreatedAt((customerRepository.findById(customer.getId()).orElse(null).getCreatedAt()));
            customer.setStatus("updated");
        }
        customer.setUpdatedAt(new Date());
        return customerRepository.save(customer);
    }

    @Override
    @Transactional
    public Customer delete(Customer customer) {
        customer.setStatus("deleted");
        customer.setCreatedAt((customerRepository.findById(customer.getId()).orElse(null).getCreatedAt()));
        customer.setUpdatedAt(new Date());
        return customerRepository.save(customer);
    }

    @Override
    @Transactional(readOnly = true)
    public Customer findCustomerByTypeAndNumberIdentification(TypeIdentification typeIdentification, String number) {
        return customerRepository.findByTypeIdentificationAndNumberIdentification(typeIdentification, number);
    }

    @Override
    @Transactional(readOnly = true)
    public List<CustomerDTO> getAllCustomerLessThanOrGreaterThanOrEqual(String filter){
        List<Customer> customers = new ArrayList<>();
        int age = Validate.validateIfAgeHaveSpecialFormat(filter);
        if(filter.startsWith("+")){
            // +18
            customers = customerRepository.findByAgeGreaterThan(age);
        }else if (filter.startsWith("-")){
            // -18
            customers = customerRepository.findByAgeLessThan(age);
        }else if(filter.endsWith("+")){
            // 18+
            customers = customerRepository.findByAgeGreaterThanEqual(age);
        }else if(filter.endsWith("-")){
            // 18-
            customers = customerRepository.findByAgeLessThanEqual(age);
        }else {
            customers = customerRepository.findByAge(age);
        }
        return CustomerDTO.formatListCustomerToDTO(customers);
    }
}
